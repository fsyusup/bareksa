<?php

namespace App\Http\Controllers\API;

use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();
        try {

            $query = Cache::remember("topics", 30 * 60, function () {
                return Topic::paginate(20);
            });

            return responseSuccess($query);
        } catch (\Exception $e) {
            return responseSuccess($data ?? [], $e->getMessage());
        }
    }

    public function show(Request $request, $id)
    {
        $data = $request->all();
        try {

            $query = Cache::remember("topic_" . $id, 30 * 60, function () use ($id) {
                return Topic::where('id', $id)->first();
            });

            if ($query) {
                return responseSuccess($query);
            }

            return responseFailed([], 'No data  ');
        } catch (\Exception $e) {
            return responseSuccess($data ?? [], $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:topics|max:50'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $bodyReq = [
                'name' => $data['name']
            ];

            $query = Topic::insert($bodyReq);
            if ($query) {
                pull(["topics"]);
                return responseSuccess($data);
            }

            return responseFailed($data);
        } catch (\Exception $e) {

            return responseFailed($data, $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        try {
            $validator = Validator::make($data, [
                'id' => 'required|exists:topics,id',
                'name' => 'required|max:50'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $bodyReq = [
                'name' => $data['name']
            ];

            $query = Topic::where('id', $id)->update($bodyReq);
            if ($query) {
                pull(["topics", "topic_$id"]);
                return responseSuccess($data);
            }

            return responseFailed($data);
        } catch (\Exception $e) {

            return responseFailed($data, $e->getMessage());
        }
    }

    public function destroy(Request $request, $id)
    {
        $data = $request->all();
        try {
            $data['id'] = $id;
            $validator = Validator::make($data, [
                'id' => 'required|exists:topics,id'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $query = Topic::where('id', $id)->delete();
            if ($query) {
                pull(["topics", "topic_$id"]);
                return responseSuccess($data);
            }

            return responseFailed($data);
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }
}
