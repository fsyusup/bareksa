<?php

namespace App\Http\Controllers\API;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use PDO;

class NewsController extends Controller
{
    public function index(Request $request)
    {

        $data = $request->all();
        try {

            $topic_id = $request->input('topic_id', '');
            $status = $request->input('status', '');
            $params = [
                'topic_id' => $topic_id,
                'status' => $status
            ];

            foreach (array_keys($params) as $param) {
                if (request()->has($param)) {
                    $params[$param] = request()->input($param);
                }
            }
            $prefix = 'news';
            $hashed = md5(json_encode($params));
            $key = $prefix . !empty($hashed) ? '_' . $hashed : '';

            $query = Cache::tags(['news'])->remember($key, 30 * 60, function () use ($topic_id, $status) {
                return News::when($topic_id, function ($query) use ($topic_id) {
                    return $query->where('topic_id', $topic_id);
                })
                    ->when($status, function ($query) use ($status) {
                        return $query->where('status', $status);
                    })
                    ->paginate(20);
            });
            return responseSuccess($query);
        } catch (\Exception $e) {
            return responseSuccess($data ?? [], $e->getMessage());
        }
    }

    public function show(Request $request, $id)
    {
        $data = $request->all();
        try {

            $query = Cache::remember("news_" . $id, 30 * 60, function () use ($id) {
                return News::where('id', $id)->first();
            });

            if ($query) {
                return responseSuccess($query);
            }

            return responseFailed([], 'No data');
        } catch (\Exception $e) {
            return responseSuccess($data ?? [], $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:150',
                'body' => 'required',
                'topic_id' => 'required|integer|min:1|exists:topics,id',
                'tags'    => 'array'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $bodyReq = [
                'title' => $data['title'],
                'body' => $data['body'],
                'topic_id' => $data['topic_id'],
                'tags' => $data['tags'] ?? []
            ];

            $query = News::insertGetId($bodyReq);
            if ($query) {
                pull(["news"]);
                $data['id'] = $query;
                return responseSuccess($data);
            }
            return responseFailed($data);
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        try {
            $validator = Validator::make($data, [
                'id' => 'required|exists:news,id',
                'title' => 'required|max:150',
                'body' => 'required',
                'topic_id' => 'required|integer|min:1|exists:topics,id',
                'tags'    => 'array'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $bodyReq = [
                'title' => $data['title'],
                'body' => $data['body'],
                'topic_id' => $data['topic_id'],
                'tags' => isset($data['tags']) ? $data['tags'] : [],
                'status' => $data['status']
            ];

            $query = News::where('id', $id)->update($bodyReq);
            if ($query) {
                pull(["news", "news_$id"]);
                Cache::tags(['news'])->flush();
                return responseSuccess($data);
            }

            return responseFailed($data);
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }

    public function destroy(Request $request, $id)
    {
        $data = $request->all();
        try {
            $data['id'] = $id;
            $validator = Validator::make($data, [
                'id' => 'required|exists:news,id'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $query = News::where('id', $id)->delete();
            if ($query) {
                pull(["news", "news_$id"]);
                Cache::tags(['news'])->flush();
                return responseSuccess($data);
            }

            return responseFailed($data);
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }
}
