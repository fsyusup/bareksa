<?php

use Illuminate\Support\Facades\Cache;

if (!function_exists('responseFailed')) {
    function responseFailed($data = [], $message = 'FAILED', $code = 200)
    {
        return response()->json(
            [
                'status' => false,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}

if (!function_exists('responseSuccess')) {
    function responseSuccess($data = [], $message = 'OK', $code = 200)
    {
        return response()->json(
            [
                'status' => true,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}

if (!function_exists('pull')) {
    function pull($data = [])
    {
        foreach ($data as $key => $value) {
            Cache::pull($value);
        }
    }
}
