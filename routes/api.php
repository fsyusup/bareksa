<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

$router->group(['namespace' => 'API'], function () use ($router) {
    Route::resource('news', 'NewsController')->except([
        'create', 'edit'
    ]);
    Route::resource('topic', 'TopicController')->except([
        'create', 'edit'
    ]);
});
