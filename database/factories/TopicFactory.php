<?php

namespace Database\Factories;

use Faker\Generator as Faker;

$factory->define(\App\Models\Topic::class, function (Faker $faker) {
    return [
        'name' => $faker->words(5, true)
    ];
});
