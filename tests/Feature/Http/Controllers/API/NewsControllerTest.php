<?php

namespace Tests\Feature\Http\Controllers\API;

use App\Models\News;
use App\Models\Topic;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker\Generator as Faker;

class NewsControllerTest extends TestCase
{
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    public function it_stores_data()
    {

        $news = factory(News::class)->create();
        $topic = factory(CategoryModel::class)->create();
        $response = $this->actingAs($news)
            ->post(route('product.store'), [
                'title' => $this->faker->words(3, true),
                'topic_id' => $topic->id,
                'body' => $this->faker->paragraph(),
                'tags' => [],
                'status' => '',
            ]);
        $response->assertStatus(200);

        return responseSuccess($response);
    }
}
