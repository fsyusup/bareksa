Copy `.env.example` to `.env`.

Run commands  .

```shell
# Install Project
composer install

# Migration Database
php artisan migrate
```

Rest API

```ruby
GET     "/api/news"
GET     "/api/news/{id}"
PUT     "/api/news/{id}"
DELETE  "/api/news/{id}"

GET     "/api/topic"
GET     "/api/topic/{id}"
PUT     "/api/topic/{id}"
DELETE  "/api/topic/{id}"
```